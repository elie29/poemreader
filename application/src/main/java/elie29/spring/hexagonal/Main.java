package elie29.spring.hexagonal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class Main
{
   public static void main(final String[] args)
   {
      log.info("Started Console Application");
      SpringApplication.run(Main.class, args);
   }
}
