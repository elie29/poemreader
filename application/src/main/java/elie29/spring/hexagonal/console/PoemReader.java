package elie29.spring.hexagonal.console;

import elie29.spring.hexagonal.service.ConsoleAdapter;
import elie29.spring.hexagonal.service.IObtainPoems;
import elie29.spring.hexagonal.service.IRequestVerses;
import elie29.spring.hexagonal.service.PoetryLibraryFileAdapter;
import elie29.spring.hexagonal.service.PoetryReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PoemReader
{

   @EventListener(ContextRefreshedEvent.class)
   public void start()
   {
      // 1. Instantiate right-side adapter(s) ("I want to go outside the hexagon")
      final IObtainPoems fileAdapter = new PoetryLibraryFileAdapter("rimbaud.txt");

      // 2. Instantiate the hexagon
      final IRequestVerses poetryReader = new PoetryReader(fileAdapter);

      // 3. Instantiate the left-side adapter(s) ("I want ask/to go inside the hexagon")
      final ConsoleAdapter consoleAdapter = new ConsoleAdapter(poetryReader);
      consoleAdapter.ask();
   }
}
