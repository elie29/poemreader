package elie29.spring.hexagonal.service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ConsoleAdapter
{
   private final IRequestVerses poetryReader;

   public void ask()
   {
      System.out.println(poetryReader.readPoem());
   }
}
