package elie29.spring.hexagonal.service;

import lombok.RequiredArgsConstructor;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PoetryLibraryFileAdapter implements IObtainPoems
{
   private final String filename;

   @Override
   public String fetch()
   {
      try {
         final URL url = getClass().getClassLoader().getResource(filename);
         final Path path = Path.of(url.toURI());
         return Files.lines(path).collect(Collectors.joining(System.lineSeparator()));
      } catch (final Exception e) {
         e.printStackTrace();
      }
      return null;
   }
}
