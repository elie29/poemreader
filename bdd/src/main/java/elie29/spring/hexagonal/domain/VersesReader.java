package elie29.spring.hexagonal.domain;

public interface VersesReader
{
   String giveMeSomePoem();
}
