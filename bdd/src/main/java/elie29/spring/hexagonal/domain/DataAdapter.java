package elie29.spring.hexagonal.domain;

public interface DataAdapter
{
   String readPoem();
}
