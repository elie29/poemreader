package elie29.spring.hexagonal.domain;

import elie29.spring.hexagonal.infrastructure.FixedDataAdapter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PoetryReader implements VersesReader
{
   @NonNull
   private final DataAdapter dataAdapter;

   public PoetryReader()
   {
      this.dataAdapter = new FixedDataAdapter();
   }

   @Override
   public String giveMeSomePoem()
   {
      return dataAdapter.readPoem();
   }
}
