package elie29.spring.hexagonal.infrastructure;

import elie29.spring.hexagonal.domain.DataAdapter;
import lombok.RequiredArgsConstructor;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class FilePoemAdapter implements DataAdapter
{
   private final String filename;

   @Override
   public String readPoem()
   {
      try {
         final URL url = getClass().getClassLoader().getResource(filename);
         final Path path = Path.of(url.toURI());
         return Files.lines(path).collect(Collectors.joining(System.lineSeparator()));
      } catch (final Exception e) {
         e.printStackTrace();
      }
      return null;
   }
}
