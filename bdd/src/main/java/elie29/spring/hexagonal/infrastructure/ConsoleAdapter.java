package elie29.spring.hexagonal.infrastructure;

import elie29.spring.hexagonal.domain.VersesReader;
import lombok.RequiredArgsConstructor;

import java.io.PrintStream;

@RequiredArgsConstructor
public class ConsoleAdapter
{

   private final VersesReader versesReader;
   private final PrintStream out;

   public void print()
   {
      // Business logic from domain
      final String verses = versesReader.giveMeSomePoem();

      // from domain to infra
      out.println(verses);
   }
}
