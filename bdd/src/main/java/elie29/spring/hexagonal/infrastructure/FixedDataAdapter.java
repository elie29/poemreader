package elie29.spring.hexagonal.infrastructure;

import elie29.spring.hexagonal.domain.DataAdapter;

public class FixedDataAdapter implements DataAdapter
{
   @Override
   public String readPoem()
   {
      return "Where is my verses";
   }
}
