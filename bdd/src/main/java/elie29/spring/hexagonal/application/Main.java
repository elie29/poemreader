package elie29.spring.hexagonal.application;

import elie29.spring.hexagonal.domain.DataAdapter;
import elie29.spring.hexagonal.domain.PoetryReader;
import elie29.spring.hexagonal.domain.VersesReader;
import elie29.spring.hexagonal.infrastructure.ConsoleAdapter;
import elie29.spring.hexagonal.infrastructure.FilePoemAdapter;
import elie29.spring.hexagonal.infrastructure.FixedDataAdapter;

public class Main
{
   public static void main(final String[] args)
   {
      // 1. Instantiate right-side adapter(s) ("I want to go outside the hexagon")
      final DataAdapter dataAdapter = new FilePoemAdapter("rimbaud.txt");
      final DataAdapter fixedDataAdapter = new FixedDataAdapter();

      // 2. Instantiate the hexagon
      final VersesReader versesReader1 = new PoetryReader(dataAdapter);
      final VersesReader versesReader2 = new PoetryReader(fixedDataAdapter);

      // 3. Instantiate the left-side adapter(s) ("I want ask/to go inside the hexagon")
      final ConsoleAdapter consoleAdapter1 = new ConsoleAdapter(versesReader1, System.out);
      final ConsoleAdapter consoleAdapter2 = new ConsoleAdapter(versesReader2, System.out);

      System.out.println("Here is some...");
      consoleAdapter1.print();
      consoleAdapter2.print();

      System.out.println("The end...");
   }
}
