package elie29.spring.hexagonal;

import elie29.spring.hexagonal.domain.DataAdapter;
import elie29.spring.hexagonal.domain.PoetryReader;
import elie29.spring.hexagonal.domain.VersesReader;
import elie29.spring.hexagonal.infrastructure.ConsoleAdapter;
import elie29.spring.hexagonal.infrastructure.FilePoemAdapter;
import org.junit.Test;

import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.times;
import static org.mockito.Mockito.mock;

public class ApplicationTest
{

   @Test
   // null: Right side port -> Infrastructure Data Adapter
   // VersesReader: Left side port -> Application adapter
   // PoetryReader: Hexagon -> Domain
   public void should_give_verses_when_asked_for_poetry()
   {
      // given
      final VersesReader versesReader = new PoetryReader();

      // when
      final String verses = versesReader.giveMeSomePoem();

      // then
      assertThat(verses, containsString("my verses"));
   }

   @Test
   // DataAdapter: Right side port -> FilePoemAdapter is Infrastructure Data Adapter
   // VersesReader: Left side port used by any Application adapter: VersesReader is just to limit API access
   // PoetryReader: Hexagon -> Domain
   public void should_give_verses_with_mocked_adapter()
   {
      // given
      final DataAdapter dataAdapter = mock(DataAdapter.class);
      given(dataAdapter.readPoem()).willReturn("I love you too");

      final VersesReader versesReader = new PoetryReader(dataAdapter);
      // when
      final String verses = versesReader.giveMeSomePoem();

      // then : dataAdapter is a mock
      then(dataAdapter).should(times(1)).readPoem();
      assertThat(verses, containsString("I love you too"));
   }

   @Test
   // DataAdapter: Right side port -> FilePoemAdapter is Infrastructure Data Adapter
   // VersesReader: Left side port used by any Application adapter: VersesReader is just to limit API access
   // PoetryReader: Hexagon -> Domain
   public void should_give_verses_with_file_adapter()
   {
      // given
      final DataAdapter dataAdapter = new FilePoemAdapter("rimbaud.txt");
      final VersesReader versesReader = new PoetryReader(dataAdapter);

      // when
      final String verses = versesReader.giveMeSomePoem();

      // then
      assertThat(verses, containsString("I love you"));
   }

   @Test
   // DataAdapter: Right side port -> FilePoemAdapter is Infrastructure Data Adapter
   // VersesReader: Left side port used by a console adapter: VersesReader is just to limit API access
   // PoetryReader: Hexagon -> Domain
   public void should_write_verses_to_console_with_file_adapter()
   {
      // given
      final DataAdapter dataAdapter = new FilePoemAdapter("rimbaud.txt");
      final VersesReader versesReader = new PoetryReader(dataAdapter);
      final PrintStream out = mock(PrintStream.class);

      // when
      final ConsoleAdapter console = new ConsoleAdapter(versesReader, out);
      console.print();

      // then data should be printed out
      then(out).should(times(1)).println(anyString());
   }
}

