package elie29.spring.hexagonal.service;

public interface IObtainPoems
{
   String fetch();
}
