package elie29.spring.hexagonal.service;

public interface IRequestVerses
{
   String readPoem();
}
