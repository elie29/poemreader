package elie29.spring.hexagonal.service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PoetryReader implements IRequestVerses
{
   private final IObtainPoems fileAdapter;

   @Override
   public String readPoem()
   {
      return fileAdapter.fetch();
   }
}
